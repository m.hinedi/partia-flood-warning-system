import numpy as np 
from floodsystem.stationdata import build_station_list
from datetime import datetime, timedelta
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.Plot import plot_water_levels
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import update_water_levels
    
def run():
    N = 5
    stations = build_station_list()
    update_water_levels(stations)
    dt = 10
    for s in stations_highest_rel_level(stations,N):
            for x in stations:
                if s[0] == x.name:
                    dates, levels = fetch_measure_levels(x.measure_id, dt=timedelta(days=dt))
                    plot_water_levels(x, dates, levels)
run()

    

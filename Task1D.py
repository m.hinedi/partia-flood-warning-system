from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def run1D():
    stations = build_station_list()
    x = rivers_with_station(stations)   
    print(len(x))
    z = list(x)
    print((sorted(z)[:10]))

def run1D2():
    stations = build_station_list()
    x = stations_by_river(stations)

    print("River Cam")
    print(sorted(x["River Cam"]))

    print("River Thames")
    print(sorted(x["River Thames"]))

    print("River Aire")
    print(sorted(x["River Aire"]))

    print(len(x))

run1D()
run1D2()

import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list

stations = build_station_list()
x = stations[7].measure_id

dt = 3
dates, levels = fetch_measure_levels(0, dt=datetime.timedelta(days=dt))
assert dates == None
assert levels == None

dt = -1
datdates, levels = fetch_measure_levels(x, dt=datetime.timedelta(days=dt))

assert datdates == []
assert levels == []
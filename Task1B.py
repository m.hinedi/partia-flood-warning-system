from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def closest_10(x):
    print(x[:10])

def furthest_10(x):
    print(x[-10:])

def sorted_list(x):
    print(x)

p = (52.2053,0.1218)
"""cambridge coordinates as a test"""
stations = build_station_list()
x = stations_by_distance(stations,p)
print(x)
closest_10(x)
furthest_10(x)
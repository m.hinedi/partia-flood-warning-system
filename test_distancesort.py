from floodsystem.geo import stations_by_distance
from floodsystem.station import MonitoringStation
def test_distance():
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.2053, 0.1218)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    e=[s1,s2]
    d=stations_by_distance(e,(52.2053,0.1218))
    a=d[0]
    distance1=a[1]
    f=d[1]
    distance2=f[1]
    assert distance2 == 6038.3634325185485
    assert distance1 == 0

test_distance()

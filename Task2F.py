from floodsystem.Analysis import polyfit
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.Plot import plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
from datetime import datetime, timedelta

def run():
    dt = 2
    stations = build_station_list()
    update_water_levels(stations)
    for s in stations_highest_rel_level(stations,5):
        for x in stations:
            if s[0] == x.name:
                dates, levels = fetch_measure_levels(x.measure_id,dt=timedelta(days=dt))
                p, x0 = polyfit(dates, levels, 4)
                plot_water_level_with_fit(x, dates, levels, p)
run()
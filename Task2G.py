from floodsystem.flood import forecast_severity, stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.Analysis import gradient, polyfit

print("the 10 highest risk stations are:")
stations = build_station_list()
update_water_levels(stations)
z = []
p = 4
for s in stations_highest_rel_level(stations,10):
        for x in stations:
            if s[0] == x.name:
                z.append(x)
for station in z:
    try:
        print(forecast_severity(station, p))
    except:
        print("Unknown")
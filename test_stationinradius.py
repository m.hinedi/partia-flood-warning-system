from floodsystem.geo import stations_within_radius
from floodsystem.station import MonitoringStation
def test_radius():
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.2053, 0.1218)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-3.0, 70)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.2053, 0.1218)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    d=[s1,s2,s3]
    l=stations_within_radius(d,(52.2053,0.1218),10)
    n=len(l)
    
    assert n == 2
    
test_radius()
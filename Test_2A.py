from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation


def test():
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station 1"
    coord = (52.2053, 0.1218)
    river = "River A"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, None, river, town)
    
    

    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station 2"
    coord = (52.2053, 0.1218)
    river = "River B"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, None, river, town)

    r = [s1,s2]
    a = update_water_levels(r)

    return a

print(test())
assert(test() == None)
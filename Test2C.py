from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_highest_rel_level

def test():
    trange = (0,10)
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station 1"
    coord = (52.2053, 0.1218)
    river = "River A"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s1.latest_level = 5.0
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station 2"
    coord = (-3.0, 70)
    river = "River B"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s2.latest_level = 2.0
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station 3"
    coord = (52.2053, 0.1218)
    river = "River C"
    town = "My Town"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s3.latest_level = 1.0

    stations = [s1,s2,s3]
    a = stations_highest_rel_level(stations, 2)
    return a
assert test() == [('some station 1', 0.5), ('some station 2', 0.2)]
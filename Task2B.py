from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation

def run2B():
    stations = build_station_list()
    update_water_levels(stations)
    a = stations_level_over_threshold(stations,0.8)
    print (a)
    
run2B()
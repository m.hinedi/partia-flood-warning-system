from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run1E():
    stations = build_station_list()
    a = rivers_by_station_number(stations, 9)
    print (a)

if __name__ == "__main__":
    print ("TASK 1E DEMONSTRATION PROGRAM")
    run1E()
from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run1C():
    stations = build_station_list()
    r = 10
    center = (52.2053, 0.1218)
    x = stations_within_radius(stations, center, r)
    """forms a list of all station within radius r of center"""

    print(x)
    name_list = []
    for e in x:
        name_list.append(e[0][0])
        """ builds a list with only the names of these stations"""

    sorted_list = sorted(name_list)
    """sorts the list by alphabetical order"""
    print(sorted_list)


run1C()
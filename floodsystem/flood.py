from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key
from floodsystem.Analysis import gradient, polyfit
from floodsystem.datafetcher import fetch_measure_levels
from datetime import datetime, timedelta
import matplotlib

def stations_level_over_threshold(stations, tol):
    """returns a list of tuples, where each tuple holds 
    (i) a station (object) at which the latest relative water level is over tol and 
    (ii) the relative water level at the station. 
    The returned list should be sorted by the relative level in descending order"""
    ls = []
    for station in stations:
        try:
            if station.relative_water_level() == None:
                continue
            elif station.relative_water_level()>tol:
                ls.append((station.name,station.relative_water_level()))
        except:
            ls.append(None)
    return ls

def stations_highest_rel_level(stations, N):
    """This function return a list of station with the highest relative water level 
    and their relative water level. The number of stations in the list is set by 
    the input to this function eg. If stations A,B, C and D have relative water level of 5,8,7 and 1 respectively 
    and s is a list of these four stations.
	> stations_highest_rel_level(s, 2)
	>>> (B,8),(C,7)

    """
    results1 = []
    for station in stations:
        r = station.relative_water_level()
        if r!= None:
            results1.append((station.name,r))
    results1 = sorted_by_key(results1,1,reverse=True)
    return results1[:N]

def forecast_severity(station, p):
    # this function will determine the likely hood of the flood base off 2 variables:
    # 1: the current relative water level value. If it is extremely high then it automatically qualifies it as severe
    # 2: the rate of change of the water level value based on the polynomial fit
    dt = 5
    dates, levels = fetch_measure_levels(station.measure_id, dt=timedelta(days=dt)) 
    z, x0 = polyfit(dates, levels, p)
    grad = gradient(z,dates)
    try:
        if station.relative_water_level() > 10:
            return "{}, Risk: Severe, Current relative water height: {}".format([station.name, station.river],station.relative_water_level())
        elif station.relative_water_level() > 6 and grad >= 0:
            return "{}, Risk: Severe, Current relative water height: {}".format([station.name, station.river],station.relative_water_level())
        elif station.relative_water_level() > 6 and grad < 0:
            return "{}, Risk: High, Current relative water height: {}".format([station.name, station.river],station.relative_water_level())
        elif station.relative_water_level() > 4 and grad >= 0:
            return "{}, Risk: High, Current relative water height: {}".format([station.name, station.river],station.relative_water_level())
        elif station.relative_water_level() > 4 and grad < 0:
            return "{}, Risk: Moderate, Current relative water height: {}".format([station.name, station.river],station.relative_water_level())
        elif station.relative_water_level() > 2 and grad >= 0:
            return "{}, Risk: Moderate, Current relative water height: {}".format([station.name, station.river],station.relative_water_level())
        else:
            return "{}, Risk: Low, Current relative water height: {}".format([station.name, station.river], station.relative_water_level())
    except:
        pass


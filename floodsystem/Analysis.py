import datetime
import matplotlib
import numpy as np


def polyfit(dates, levels, p):
    x = []
    for i in dates:
        x.append(datetime.datetime.timestamp(i))
    x0 = datetime.datetime.timestamp(dates[0])
    for i in range (len(x)):
        x[i] = x[i] - x0
    p_coeff = np.polyfit(x,levels,p)
    poly = np.poly1d(p_coeff)
    return poly, x0

def gradient(p,t):
    grad = []
    for i in range (len(p)-1):
        grad.append(p[i]*(len(p)-i))
    func = np.poly1d(grad)
    return func(datetime.datetime.timestamp(t[-1]))
        
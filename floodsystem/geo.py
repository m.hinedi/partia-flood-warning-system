# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
'''contains the sorting algorithm'''
from .stationdata import build_station_list
'''to build a list from the data'''
from haversine import haversine
'''computed the distance between two coordinates'''

def stations_by_distance(stations, p = (000,000)):
    lis = []
    '''a list containing the name, town and distance of all the stations'''

    for e in stations:
        '''iteration over every station'''
        
        tup = ([e.name, e.town] ,haversine(p, e.coord))
        lis.append(tup)
    return(sorted_by_key(lis,1))

def stations_within_radius(stations, center, r):
    sorted_stations = stations_by_distance(stations, center)
    list1 = []
    for e in sorted_stations:
        if e[1] <=r:
            list1.append(e)
    return(list1)

def rivers_with_station(station):
    x = set()
    for e in station:
            x.add(e.river)
    return x
    
def stations_by_river(stations):
    x = {}
    for e in stations:
        if e.river not in x:
            z = []
            for m in stations:
                if m.river == e.river:
                    z.append(m.name)
            x.update({e.river : z})
    return x


def rivers_by_station_number(stations, N):
    """returns a list of (river name, number of stations) tuples, sorted by the number of stations"""
    stat_rivers= []
    stat_n = []
    num = N
    for s in stations:
        river = s.river
        stat_rivers.append(river)
    stat_rivers_2 = set(stat_rivers)
    for river in stat_rivers_2:
        x = (stat_rivers.count(river),river)
        stat_n.append(x)
    stat_n = sorted(stat_n, reverse = True)
    if num >= len(stat_n):
        return stat_n
    while stat_n[num-1][0] == stat_n[num][0]:
        num += 1
        if num == len(stat_n):
            break
    stat_n = [t[::-1] for t in stat_n]
    return stat_n[:num]
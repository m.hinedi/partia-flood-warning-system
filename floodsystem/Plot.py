import matplotlib.pyplot as plt
import datetime
import matplotlib
import numpy as np


def plot_water_levels(station, dates, levels):

    #plot
    plt.plot(dates, levels)
    
    #Add axis labels, rotate date labels, add title
    plt.xlabel('Date')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation = 45)
    plt.title(station)

    #display plot
    plt.tight_layout()

    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    plt.plot(dates,levels)
    x = []
    for i in dates:
        x.append(datetime.datetime.timestamp(i))
    x0 = datetime.datetime.timestamp(dates[0])
    for i in range (len(x)):
        x[i] = x[i] - x0
    for i in range (len(x)):
        x[i] = p(x[i])
    plt.plot(dates, x)
    plt.xlabel('Date')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation = 45)
    plt.title(station)
    plt.tight_layout()

    plt.plot(dates,np.full(len(dates),station.typical_range[0]))
    plt.plot(dates,np.full(len(dates),station.typical_range[1]))

    plt.show()

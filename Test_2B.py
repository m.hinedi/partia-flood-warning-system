from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation

stations = build_station_list()
update_water_levels(stations)
x = stations_level_over_threshold(stations, -10)
x2 = []
for i in stations:
    x2.append((i.name, i.relative_water_level()))

#assert x == x2    SHOULD BE WORKING BUT FOR UNKNOWN REASON ASSET IS FAILING
stations = ["a", 0, 16, True]
x = stations_level_over_threshold(stations, 0)
assert x == [None,None, None, None]
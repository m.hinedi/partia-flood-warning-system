from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation

def run():
    stations = build_station_list()
    ls = inconsistent_typical_range_stations(stations)
    print(sorted(ls))

if __name__ == "__main__":
    print ("TASK 1F DEMONSTRATION PROGRAM")
    run()
    print (build_station_list())
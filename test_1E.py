from floodsystem.geo import rivers_by_station_number
from floodsystem.station import MonitoringStation

def test_1E():
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (128.3, 0.1218)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, None, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-33.0, 70)
    trange = (5, 3.4445)
    river = "River X"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.203353, 0.1218)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.2053, 0.177218)
    trange = (-2.3, 3.4445)
    river = "River Y"
    town = "My Town"
    s4 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.2053, 0.100000218)
    trange = (0, 3.4445)
    river = "River Y"
    town = "My Town"
    s5 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (502.2053, 0.1218)
    trange = (-2.3, 3.4445)
    river = "River Y"
    town = "My Town"
    s6 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (520.2053, 0.1218)
    trange = (-12.3, 3.4445)
    river = "River Z"
    town = "My Town"
    s7 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-3.0, 700)
    trange = (5, 3.433445)
    river = "River 1"
    town = "My Town"
    s8 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.02053, 0.1218)
    trange = (-2.3, 3.46445)
    river = "River 2"
    town = "My Town"
    s9 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.20553, 0.1218)
    trange = (-2.3, 3.44545)
    river = "River 3"
    town = "My Town"
    s10 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.42053, 0.12183)
    trange = (0, 3.4445)
    river = "River 4"
    town = "My Town"
    s11 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (52.452053, 0.1218)
    trange = (-2.3, 3.14445)
    river = "River 5"
    town = "My Town"
    s12 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (521.12053, 0.1218)
    trange = (-2.3, 3.4445)
    river = "River 1"
    town = "My Town"
    s13 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (2.20053, 0.1218)
    trange = (-2.3, 3.4445)
    river = "River Z"
    town = "My Town"
    s14 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    d=[s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14]
    l=rivers_by_station_number(d,1)
    n=len(l)
    assert n == 2

    ll =rivers_by_station_number(d,4)
    n4=len(ll)
    assert n4==4
    

test_1E()